// ----- utilities -----

const fs = require('fs').promises
const express = require('express') ;
const http = require('http') ;
const websocket = require('ws') ;
const nunjucks = require('nunjucks')
const yaml = require('yaml')
const axios = require('axios')

const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}


// ----- servers (application and sockets) -----

const app = express() ;
const server = http.createServer(app) ;
const wss = new websocket.Server({server}) ;

// ----- configuration -----

nunjucks.configure('templates', {
  autoescape : true,
  express: app
})

const yaml_read = async (filename) => {
  const file = await fs.readFile(filename, 'utf8') ;
  return yaml.parse(file)
}

const load_defaults = yaml_read('defaults.yaml') ;
const load_streamers = yaml_read('streamers.yaml') ;
const load_credentials = yaml_read('credentials.yaml') ;

// ----- twitch token -----

const get_token = async () => {
  const credentials = await load_credentials ;

  //twitch auth query parameters
  const url = "https://id.twitch.tv/oauth2/token" ;
  const payload = {
          client_id: credentials.client_id,
          client_secret: credentials.client_secret,
          grant_type: 'client_credentials'
          } ;
  const r = await axios.post(url, null, { params: payload }) ;
  return r.data ;
}

const manage_token = async () => {
  //initialize a string that will be updated at each renewal
  let token = "" ;

  //number of milliseconds in a day
  const one_day = 24 * 60 * 60 * 1000

  //infinite recursion to renew the token indefinitely when expiring
  const renew_in = async (millisecs) => {
    await sleep(millisecs) ;
    const tk = await get_token() ;
    console.log("refreshed token")

    //schedule renewal using the expiration period, at most one day
    token = tk.access_token ;

    //not waiting here to send updates in the background
    renew_in(Math.min(one_day, (tk.expires_in - 120)*1000)) ;
  }

  //initiate the token renewal loop and wait for a first token
  await renew_in(0) ;
  return token ;
}

var create_token = manage_token() ;

// ----- websockets -----

const broadcast = async (data) => {
  console.log("broadcasting") ;
  for(const client of wss.clients) {
    if (client.readyState === websocket.OPEN) {
      client.send(data) ;
    }
  }
}

// ----- listing active streams -----

//list active streams of listed streamers, filtered by title, sorted by viewers
const get_active_streams = async () => {
  //ensure that the config files are read
  const streamers = await load_streamers ;
  const defaults = await load_defaults ;
  const credentials = await load_credentials ;

  //ensure that a twitch token is available
  let token = await create_token ;

  //query parameters
  const url = "https://api.twitch.tv/helix/streams"
  let payload = {
          'user_login' : Object.keys(streamers),
          'after' : ''
          } ;
  const headers = {
          'Authorization' : `Bearer ${token}`,
          'Client-id' : credentials.client_id
          } ;

  //loop until all streamer status could be recovered
  let raw_streams = [] ;
  do {
    //grab a list of streamers
    const r = await axios.get(url, {params : payload, headers : headers}) ;
    const content = r.data ;
    raw_streams.push(...content.data) ;

    //handle pagination if too many streams for a single response
    payload.after = content.pagination.cursor ;
  } while(payload.after) ;

  //filter streams by title
  let active_streams = []
  for(const s of raw_streams) {
    //default filters
    let title_filters = Object.values(defaults.filters) ;
    //replace by streamer filters if any
    if('filters' in streamers[s.user_login]) {
      title_filters = streamers[s.user_login].filters.values() ;
    }
    //the title has to match ALL filters
    if(title_filters.every(f => s.title.match(new RegExp(f, 'i')))) {
      active_streams.push(s) ;
    }
  }
  return active_streams
}

//testing websocket reactiveness with varying data
const random_active_streamers = async () => {
  const streamers = await load_streamers ;

  let res = [] ;
  for(const s in streamers) {
    if(Math.random() > 0.9) {
      res.push({
        'user_login' : s, 
        'viewer_count' : Math.floor(Math.random() * 200), 
        'thumbnail_url' : 'https://picsum.photos/{width}/{height}'
      })
    }
  }

  res.sort((s1,s2) => {
    return s2.viewer_count - s1.viewer_count ;
  })

  return res ;
}

//manage a cahe of active streams
const manage_streams = async () => {
  let defaults = await load_defaults ;
  let streamers = await load_streamers ;
  let active_streams = [] ;

  const update = async () => {
    if(wss.clients.size > 0) {
      //switch here for testing
      const renew = await get_active_streams() ;
      //const renew = await random_active_streamers() ;

      console.log("periodic streams update") ;
      //ensure that the reference is preserved
      Object.assign(active_streams, renew, {length : renew.length}) ;
      await broadcast(JSON.stringify(active_streams)) ;
    }
  }

  await update() ;
  setInterval(update, defaults.render.interval*1000) ;
  return active_streams
}

var load_streams = manage_streams() ;

//sort streamers between streaming and offline
const sorted_streamers = async () => {
  //get currently active streams
  let streamers = await load_streamers ;
  const active_streams = await load_streams ;

  //merge active streamer meta data with their respective stream data
  let live_streamers = [] ;
  for(const s of active_streams) {
    live_streamers.push({
      ...streamers[s.user_login], login : s.user_login, stream : s
    }) ;
  }

  //list inactive streamers
  let active_logins = new Set(active_streams.map(s => s.user_login))
  let offline_streamers = []
  for(const s in streamers) {
    if(!active_logins.has(s)) {
      offline_streamers.push({...streamers[s], login : s}) ;
    }
  }

  return [live_streamers,offline_streamers]
}

wss.on('connection', async (ws) => {
  let streams = await load_streams ;
  let timer = null ;

  timer = setTimeout(() => { ws.close() }, 700000) ;
  setInterval(() => { ws.ping() }, 300000) ;

  ws.on('pong', () => {
    clearTimeout(timer) ;
    timer = setTimeout(() => { ws.close() }, 700000) ;
  }) ;

  //renew if no other connection
  if(wss.clients.size == 1) {
    const renew = await get_active_streams() ;

    //ensure that the reference is preserved
    Object.assign(streams, renew, {length : renew.length}) ;
    console.log("forced streams update") ;
  } else {
    console.log("using existing streams") ;
    console.log(wss.clients.size) ;
  }

  ws.send(JSON.stringify(streams)) ;
})

// ----- routes -----

app.get('/', async (req, res) => {
  const defaults = await load_defaults ;
  const streamers = await load_streamers ;
  res.send(nunjucks.render('base.html', {streamers : streamers, defaults : defaults})) ;

  const from = req.get('X-Forwarded-For') ;
  console.log(`connection from ${from}`) ;
})

app.use(express.static('static')) ;

// ----- launch -----

(async () => {
  const defaults = await load_defaults ;
  server.listen(defaults.server.port, defaults.server.host, async () => {
    console.log("listening")
  }) ;
})()
