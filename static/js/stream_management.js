(function(factory) {
  //namespacing
  if(!window["Streams"]) {
    window["Streams"] = {} ;
  }
  factory(window["Streams"]["explorer"]) ;
})(function(Streams) { //namespace Streams

  //hopefully refresh thumbnail images
  const update_thumbnail = async (img, url) => {
    //add salt to generate a new url
    const salt = new Date().getTime() ;
    const stamped_url = url + `#${salt}` ;
    //refetch the image
    await fetch(stamped_url, {cache : 'reload', mode : 'no-cors'}) ;
    //set src
    img.src = stamped_url ;
  }

  //move an inactive card to active
  const activate = (panel, stream, multi) => {
    //move the card to the live section
    const login = stream.user_login ;
    const online_section = document.getElementById('live-streams') ;
    online_section.append(panel) ;

    //acivate the thumbnail image
    const thumbnail = panel.querySelector(".thumbnail") ;
    thumbnail.classList.remove("d-none") ;
    const thumbnail_url = stream.thumbnail_url.replace("{width}", 256).replace("{height}", 144) ;
    update_thumbnail(thumbnail, thumbnail_url) ;

    //switch stream button to stream
    const btn = panel.querySelector(".stream-btn") ;
    btn.classList.remove('btn-outline-primary') ;
    btn.classList.add('btn-primary') ;
    btn.querySelector("span").innerHTML = "Stream" ;

    //fill in viewer count
    const counter = panel.querySelector(".viewer-count") ;
    counter.innerHTML = `Viewers : ${stream.viewer_count}` ;

    //handle multitwitch checkbox
    const multitwitch = panel.querySelector(".multitwitch-section") ;
    if(multi) {
      multitwitch.classList.remove("d-none") ;
    } else {
      multitwitch.classList.add("d-none") ;
      //ensure that unchecked
      const checkbox = multitwitch.querySelector(".multitwitch-select") ;
      checkbox.checked = false ;
    }
  }

  //update a card thats already active
  const update = (panel, stream) => {
    //move the card still in the live section to ensure sorting
    const login = stream.user_login ;
    const online_section = document.getElementById('live-streams') ;
    online_section.append(panel) ;

    //force update of the thumbnail
    const thumbnail = panel.querySelector(".thumbnail") ;
    thumbnail.classList.remove("d-none") ;
    const thumbnail_url = stream.thumbnail_url.replace("{width}", 256).replace("{height}", 144) ;
    update_thumbnail(thumbnail, thumbnail_url) ;

    //update viewer counts
    const counter = panel.querySelector(".viewer-count") ;
    counter.innerHTML = `Viewers : ${stream.viewer_count}` ;

    //handle multitwitch checkbox
    const multitwitch = panel.querySelector(".multitwitch-section") ;
    if(multi) {
      multitwitch.classList.remove("d-none") ;
    } else {
      multitwitch.classList.add("d-none") ;
    }
  }

  //move an active card to inactive
  const deactivate = (panel) => {
    //move the card to the offline section
    //insert front to increase visibility of recently disconnected
    const offline_section = document.getElementById('offline-streamers') ;
    offline_section.prepend(panel) ;

    //deactivate thumbnail image
    const thumbnail = panel.querySelector(".thumbnail") ;
    thumbnail.classList.add("d-none") ;
    thumbnail.src = "" ;

    //switch stream button to redif
    const btn = panel.querySelector(".stream-btn") ;
    btn.classList.add('btn-outline-primary') ;
    btn.classList.remove('btn-primary') ;
    btn.innerHTML = "Redif" ;

    //deactive viewer count
    const counter = panel.querySelector(".viewer-count") ;
    counter.innerHTML = "Offline" ;

    //deactivate multutwitch check
    const multitwitch = panel.querySelector(".multitwitch-section") ;
    multitwitch.classList.add("d-none") ;
    //ensure that unchecked
    const checkbox = multitwitch.querySelector(".multitwitch-select") ;
    checkbox.checked = false ;
  }

  //general update on streamers update
  const update_online = (active_streams) => {
    //visibility of multitwitch button based on number of viewers
    const multi = active_streams.length > 1 ;
    const multi_btn = document.getElementById('multitwitch-btn') ;
    if(multi) {
      multi_btn.classList.remove('d-none') ;
    } else {
      multi_btn.classList.add('d-none') ;
    }

    //handle active streams
    const online_section = document.getElementById('live-streams') ;
    for(const stream of active_streams) {
      const panel = document.getElementById(`panel-${stream.user_login}`) ;
      if(panel.parent === online_section) {
        //the stream card is already in the live section
        update(panel, stream, multi) ;
      } else {
        //new live stream
        activate(panel, stream, multi) ;
      }
    }

    //handle disconnections
    const online_panels = online_section.querySelectorAll('.streamer') ;
    if(online_panels.length != active_streams.length) {
      //reference all active logins
      const logins = new Set(active_streams.map(n => n.user_login)) ;
      for(const panel of online_panels) {
        //check each active card wrt. the active logins
        const login = panel.id.replace('panel-', '') ;
        if(!logins.has(login)) {
          //move the card to the offline section
          deactivate(panel) ;
        }
      }
    }
  }

  //websocket connection
  const connect = () => {
    //forge socket url
    const loc = window.location ;
    const protocol = loc.protocol === "https:" ? "wss" : "ws" ;

    console.log(`connecting to ${protocol}://${loc.host}`)
  
    //reconnection after 5s of disconnection or error
    const reconnect = () => {
      setTimeout(connect, 5000) ;
    }

    //initializing the socket
    let sk = null ;
    try {
      sk = new WebSocket(`${protocol}://${loc.host}`) ;
    } catch(err) {
      console.log(`failed opening stream socket ${err}`) ;
    }

    //on error, log and reconnect
    sk.onerror = (err) => {
      console.log(`stream socket got error ${err}`) ;
      reconnect() ;
    }

    sk.onopen = () => {
      //on message, log and update
      sk.onmessage = (evt) => {
        console.log(`stream socket received ${evt.data}`) ;
        update_online(JSON.parse(evt.data)) ;
      }

      //on close, log and reconnect
      sk.onclose = (evt) => {
        console.log("lost stream socket connection, trying to reconnect") ;
        reconnect() ;
      }
    }
  }

  //launch connection once the DOM is filled
  document.addEventListener("DOMContentLoaded", connect) ;
})
